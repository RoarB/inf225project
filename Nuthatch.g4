// Define a grammar called Hello
grammar Nuthatch;
walk : 'walk' closure ;
closure : ID paras? '{' ('state' declaration)* statement+ '}' ;
paras : '(' ID (',' ID)* ')' ;

statement : '{' statement+ '}'
| 'if' expression 'then' statement ('else' statement)?
| declaration
| ID '=' expression ';'
| expression ';'
| 'return' expression ';'
| 'walk' 'to' expression ';'
| 'stop' ';'
| 'suspend' ';'
| '!' term ';'
| function  // ??
| action  // ??
;

declaration : type? ID '=' expression ';' ;

// Should have Java style expressions as well
expression :
'?' term
| getter
| '~' ID paras? ;

action : 'action' closure ;

function : 'function' closure ;


// These are very uncertain
type : 'int'
| 'string'
;

term : 'Add'
| 'Mult'
;

getter : 'arity'
| 'data'
| 'parent'
| 'next'
;


ID : [a-z]+ ;
WS : [ \t\r\n]+ -> skip ;